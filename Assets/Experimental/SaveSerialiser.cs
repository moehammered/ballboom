﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

public class SaveSerialiser
{
    private static SaveSerialiser instance;
    private BinaryFormatter formatter;
    private MemoryStream memoryStream;

    protected SaveSerialiser()
    {
        //default constructor
        formatter = new BinaryFormatter();
    }

    public T loadData<T>(string tag)
    {
        object data = null;
        string temp = PlayerPrefs.GetString(tag, string.Empty);
        if (temp == string.Empty)
        {
            data = "";
            return default(T);
        }
        else
        {
            using (memoryStream = new MemoryStream(System.Convert.FromBase64String(temp)))
            {
                data = formatter.Deserialize(memoryStream);
            }
        }

        return (T)data;
    }

    public object loadData(string tag)
    {
        object data = null;
        string temp = PlayerPrefs.GetString(tag, string.Empty);
        if (temp == string.Empty)
        {
            data = "";
        }
        else
        {
            using (memoryStream = new MemoryStream(System.Convert.FromBase64String(temp)))
            {
                data = formatter.Deserialize(memoryStream);
            }
        }

        return data;
    }

    public void saveData(string tag, object obj)
    {
        using (memoryStream = new MemoryStream())
        {
            formatter.Serialize(memoryStream, obj);
            string temp = System.Convert.ToBase64String(memoryStream.ToArray());
            PlayerPrefs.SetString(tag, temp);
            //Debug.Log(this + ": Converting " + obj + " to " + temp);
        }
    }
    
    public static SaveSerialiser getInstance()
    {
        if(instance == null)
        {
            instance = new SaveSerialiser();
        }

        return instance;
    }
}
