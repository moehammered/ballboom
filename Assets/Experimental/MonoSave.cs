﻿using UnityEngine;
using System.Collections;

public class MonoSave : MonoBehaviour {

    public const string nameTag = "name_player", healthTag = "health_player",
                        scoreTag = "score_player", finishedTag = "finished_player";
    public string playerName = "Mick";
    public int score = 10;
    public bool finished = false, saveNow, loadNow;
    public float playerHealth = 100;

    private void savePlayerInfo()
    {
        SaveSerialiser.getInstance().saveData(nameTag, playerName);
        SaveSerialiser.getInstance().saveData(healthTag, playerHealth);
        SaveSerialiser.getInstance().saveData(scoreTag, score);
        SaveSerialiser.getInstance().saveData(finishedTag, finished);
    }

    private void loadPlayerInfo()
    {
        //loadPlayerType<string>(nameTag, ref playerName);
        //loadPlayerType<float>(healthTag, ref playerHealth);
        //loadPlayerType<bool>(finishedTag, ref finished);
        //loadPlayerType<int>(scoreTag, ref score);
        playerName = (string)SaveSerialiser.getInstance().loadData<string>(nameTag);
        playerHealth = (float)SaveSerialiser.getInstance().loadData<float>(healthTag);
        score = (int)SaveSerialiser.getInstance().loadData<int>(scoreTag);
        finished = (bool)SaveSerialiser.getInstance().loadData<bool>(finishedTag);
    }

    private void loadPlayerType<T>(string tag, ref T targetVar)
    {
        try
        {
            targetVar = (T)SaveSerialiser.getInstance().loadData(tag);
        } catch(System.InvalidCastException e)
        {
            Debug.LogError("Error converting your save data back from binary.");
            Debug.LogError(e.Message);
            Debug.LogError("Here's the stack trace.");
            Debug.LogError(e.StackTrace);
            Debug.LogError("Common thing to check is if you gave the right save tag and right variable along with it's type!");

            //targetVar = default(T);
        }
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if(saveNow)
        {
            saveNow = false;
            savePlayerInfo();
        }
        if(loadNow)
        {
            loadNow = false;
            loadPlayerInfo();
        }

	}
}
