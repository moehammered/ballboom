﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelSelectButton : MonoBehaviour {

	public SaveManager saveManager;
	private Image btnImage;
	private Text btnText;
	public int levelNo = -1;

	void Awake ()
	{
		btnImage = GetComponent<Image>();
		btnText = GetComponentInChildren<Text>();
		if(levelNo == -1)
			levelNo = System.Convert.ToInt32(btnImage.name);
	}
	
	// Use this for initialization
	void Start () {
		colourButton();
		displayScore();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	private void colourButton ()
	{
		if (isCompleted ()) {
			btnImage.color = Color.green;
		}
	}

	private void displayScore ()
	{
		int[] scores = saveManager.getHighScores ();
		if (scores != null && scores.Length >= levelNo) {
			btnText.text += "\nScore: " + scores[levelNo == 0 ? levelNo : levelNo-1];
		}
	}

	private bool isCompleted ()
	{
		int[] comp = saveManager.getCompletedLevels ();
		if (comp != null) {
			foreach (int i in comp) {
				if (i == levelNo) {
					return true;
				}
			}
		}

		return false;
	}
}
