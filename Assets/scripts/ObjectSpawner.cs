﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.UI;

public class ObjectSpawner : MonoBehaviour {

	public bool infinite = false;
	public int nextObject = 0, objectsLeft;
	public Text ballDisp;
	public GameObject currentPlayer;
	public GameObject[] prefabs;
	public UnityEvent onEmptied;

	// Use this for initialization
	void Start () {
		objectsLeft = prefabs.Length;
		if(infinite && ballDisp)
			ballDisp.gameObject.SetActive(false);

		spawnObject();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void spawnObject ()
	{
		if (this.enabled) {
			if ((nextObject < prefabs.Length && objectsLeft > 0) || infinite) {
				GameObject obj = (GameObject)Instantiate (prefabs [nextObject], transform.position, Quaternion.identity);
				nextObject++;
				if (!infinite) {
					objectsLeft--;
				} else {
					if(nextObject >= prefabs.Length)
						nextObject = 0;
				}
				ballDisp.text = "Balls: " + objectsLeft + "/" + prefabs.Length;
				currentPlayer = obj;
			} else {
				onEmptied.Invoke ();
				this.enabled = false;
			}
		}
	}
}
