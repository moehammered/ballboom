﻿using UnityEngine;
using System.Collections;

public class ForceInput : MonoBehaviour {

	public LayerMask clickable;
	public ObjectSpawner playerSpawner;
	public Vector3 impactPoint;
	public GameObject pointMarker;
	public Color maxPowerColour = Color.red;
	public TNTSpawner spawner;
	public bool holdMode = true;
	public float radiusBuildSpd = 2, radiusBuildup = 0;
	public float minForce = 5f, currentForce = 0f, maxForce = 10f, 
		currentRadius = 0f, minRadius = 2f, maxRadius = 5f;
	private Material markerMaterial;
	private Color initialColour;

	void Awake ()
	{
		if (!playerSpawner) {
			playerSpawner = GameObject.FindObjectOfType<ObjectSpawner>();
		}
		if (!pointMarker) {
			pointMarker = GameObject.CreatePrimitive(PrimitiveType.Sphere);
			pointMarker.GetComponent<Collider>().enabled = false;
		}
		markerMaterial = pointMarker.GetComponentInChildren<Renderer>().material;
		initialColour = markerMaterial.color;
		pointMarker.SetActive(false);
	}

	// Use this for initialization
	void Start () {
		
	}

	void FixedUpdate ()
	{
		if (holdMode && Input.GetMouseButton(0)) {
			impactPoint = findGroundPoint();
		}
	}

	// Update is called once per frame
	void Update () {
		if(spawner.hasStock())
			checkInput();
	}

	void checkInput ()
	{
		if (Input.GetMouseButtonDown (0)) {
			impactPoint = findGroundPoint ();	
			currentForce = minForce;
			currentRadius = minRadius;
			radiusBuildup = 0;
			setMarker();
		} else if (Input.GetMouseButton (0)) {
			buildForce ();
			updateMarker(currentRadius);
		} else if (Input.GetMouseButtonUp (0)) {
			releaseMarker();
			spawnTNT();
			//sweepForBodies(currentRadius);
		}
	}

	private void buildForce ()
	{
		radiusBuildup += radiusBuildSpd * Time.deltaTime;
		//forceBuildup += forceBuildSpd * Time.deltaTime;
		//currentForce = Mathf.PingPong(forceBuildup, maxForce-minForce) + minForce;
		currentRadius = Mathf.PingPong(radiusBuildup, maxRadius-minRadius) + minRadius;
		currentForce = currentRadius/maxRadius * maxForce;
	}

	private void setMarker ()
	{
		pointMarker.SetActive (true);
		pointMarker.transform.position = impactPoint;
	}

	private void releaseMarker()
	{
		pointMarker.transform.position = Vector3.up * 1000;
		pointMarker.SetActive(false);
	}

	private void spawnTNT ()
	{
		/*Vector3 spawnPoint = impactPoint + Vector3.up * 10;
		GameObject obj = (GameObject)Instantiate(TNTPrefab, spawnPoint, Quaternion.identity);
		TNTExplosion tnt = obj.GetComponent<TNTExplosion>();
		tnt.setupTNT(currentForce, currentRadius);*/
		spawner.spawnTNT(impactPoint, currentForce, currentRadius);
	}

	private void updateMarker (float radius)
	{
		pointMarker.transform.position = impactPoint;
		if (playerSpawner.currentPlayer) {
			Vector3 playerPos = playerSpawner.currentPlayer.transform.position;
			playerPos.y = pointMarker.transform.position.y;
			pointMarker.transform.LookAt(playerPos);
		}
		Vector3 scale = Vector3.one * (radius*2);
		pointMarker.transform.localScale = scale;
		markerMaterial.color = Color.Lerp(maxPowerColour, initialColour,
				Mathf.PingPong(currentForce/maxForce, 0.5f));
	}

	private Vector3 findGroundPoint ()
	{
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit hit;

		if (Physics.Raycast (ray, out hit, Camera.main.farClipPlane, clickable)) {
			//print("Hit: " + hit.collider.name);
			//print("Contact Point: " + hit.point);
			return hit.point;
		}
		if(holdMode)
			return impactPoint;

		return Vector3.zero;
	}

	private void sweepForBodies (float radius)
	{
		Collider[] objs = Physics.OverlapSphere (impactPoint, radius);
		//debugRadius(radius);
		foreach (Collider col in objs) {
			Rigidbody rb = col.GetComponent<Rigidbody> ();
			if (rb) {
				rb.AddExplosionForce(currentForce, impactPoint, radius, 1, ForceMode.Impulse);
			}
		}
	}

	private void debugRadius (float radius)
	{
		GameObject[] points = new GameObject[4];
		Vector3 offset = Vector3.zero;
		for (int i = 0; i < points.Length; i++) {
			points [i] = GameObject.CreatePrimitive (PrimitiveType.Cube);
			switch (i) {
				case 0:
					offset = Vector3.right*radius;
				break;
				case 1:
					offset = Vector3.left*radius;
				break;
				case 2:
					offset = Vector3.forward*radius;
				break;
				case 3:
					offset = Vector3.back*radius;
				break;
			}
			points[i].transform.position = offset + impactPoint;
			Destroy(points[i], 5);
		}
	}
}
