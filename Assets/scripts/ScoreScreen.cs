﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreScreen : MonoBehaviour {

	public Text totalScoreDisp, timeBonusDisp, stockBonusDisp, completionBonusDisp, highscoreText;
	public TNTSpawner tntSpawner;
	public SaveManager saver;
	public float countupTimer = 4f;
	public float maxTimeForBonus = 10f;
	public float timeBonus;
	public float stockBonus;
	public float completionBonus;
	public float totalScore;

	private void calculateStockBonus ()
	{
		stockBonus = Mathf.Round(completionBonus * tntSpawner.percentageUsed());
	}

	private void calculateTimeBonus ()
	{
		timeBonus = Mathf.Round(completionBonus * maxTimeForBonus/Time.timeSinceLevelLoad);
	}

	public void calculateTotalScore ()
	{
		calculateTimeBonus();
		calculateStockBonus();
		totalScore += timeBonus + stockBonus + completionBonus;
	}

	public void updateScorePanel()
	{
		totalScoreDisp.text = "Total: " + totalScore;
		completionBonusDisp.text = "Completed: " + completionBonus;
		timeBonusDisp.text = "Time: " + timeBonus;
		stockBonusDisp.text = "TNT Left: " + stockBonus;
	}

	public void tickupScorePanels(float duration)
	{
		StartCoroutine(countupScore(duration, timeBonus, timeBonusDisp, "Time: "));
		StartCoroutine(countupScore(duration, stockBonus, stockBonusDisp, "TNT Left: "));
		StartCoroutine(countupScore(duration, completionBonus, completionBonusDisp, "Completed: "));
		StartCoroutine(countupScore(duration, totalScore, totalScoreDisp, "Total: "));
	}

	void OnEnable ()
	{
		calculateTotalScore ();
		if (saver.saveHighScore ((int)totalScore)) {
			highscoreText.gameObject.SetActive(true);
			StartCoroutine(rainbowText(highscoreText, 2));
		}
		tickupScorePanels(countupTimer);
		//updateScorePanel();
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	private IEnumerator rainbowText (Text display, float rate)
	{
		float timer = 0;
		Color startCol = display.color;
		Color newCol = new Color (Random.Range (0f, 1f), Random.Range (0f, 1f), Random.Range (0f, 1f));
		while (timer < 1) {
			timer += Time.deltaTime * rate;
			display.color = Color.Lerp (startCol, newCol, timer);
			yield return null;
			if (timer > 1) {
				startCol = display.color;
				newCol = new Color (Random.Range (0f, 1f), Random.Range (0f, 1f), Random.Range (0f, 1f));
				timer = 0;
			}
		}
	}

	private IEnumerator countupScore (float delay, float targetNum, Text display, string prepend)
	{
		float timer = 0;

		while (timer < 1) {
			timer += Time.deltaTime/delay;
			if(timer > 1)
				timer = 1;
			display.text = prepend + (int)Mathf.Lerp(0, targetNum, timer);
			yield return null;
		}
	}
}
