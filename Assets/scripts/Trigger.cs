﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

[RequireComponent(typeof(Collider))]
public class Trigger : MonoBehaviour {

	public UnityEvent onEnter, onLeave, onStay;	
	public int triggerCount = 0;
	private Collider col;

	void Awake ()
	{
		col = GetComponent<Collider>();
	}

	void OnTriggerEnter ()
	{
		if (triggerCount == -1) {
			//infinite retrigger mode
			onEnter.Invoke();
		} else if (triggerCount > 0) {
			//limited trigger amounts
			onEnter.Invoke();
			triggerCount--;
			if(triggerCount == 0)
				col.enabled = false;
		}
	}

	void OnTriggerStay ()
	{
		if (onStay.GetPersistentEventCount () > 0) {
			if (triggerCount == -1) {
				onStay.Invoke ();
			} else if (triggerCount >= 0) {
				onStay.Invoke();
			}
		}
	}

	void OnTriggerExit ()
	{
		if (onLeave.GetPersistentEventCount () > 0) {
			if (triggerCount == -1) {
				onLeave.Invoke ();
			} else if (triggerCount >= 0) {
				onLeave.Invoke();
			}
		}
	}
}
