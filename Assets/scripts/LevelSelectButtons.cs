﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor.SceneManagement;
#endif

[ExecuteInEditMode]
public class LevelSelectButtons : MonoBehaviour {
#if UNITY_EDITOR
	public bool update = false, clear = false;
	public float height = 100, indent = -200, spacing = 10, initialOffset;
	public string levelPrefix = "";
	public List<string> levelNames;
	public RectTransform levelButtonPrefab;
	public List<GameObject> buttons;

	private void getLevels ()
	{
		UnityEditor.EditorBuildSettingsScene[] scenes = UnityEditor.EditorBuildSettings.scenes;
		for (int i = 0; i < scenes.Length; i++) {
			if (scenes [i].enabled) {
				int ind = scenes [i].path.LastIndexOf ("/");
				int last = scenes [i].path.LastIndexOf (".") - 1;
				string name = scenes [i].path.Substring (ind + 1, last - ind);
				if (name.ToLower ().Contains ("level")) {
					levelNames.Add (name);
					//addLevelButton(i);
				}
			}
		}

		addButtons();
	}

	private void addButtons ()
	{
		buttons = new List<GameObject>(levelNames.Count);
		initialOffset = height + spacing;
		for (int i = 0; i < levelNames.Count; i++) {
			if(levelNames[i].ToLower().StartsWith(levelPrefix))
				buttons.Add(addLevelButton(buttons.Count, i+1));
		}
	}

	private void removeButtons ()
	{
		foreach (GameObject obj in buttons) {
			DestroyImmediate(obj);
		}
		buttons.Clear();
	}

	private GameObject addLevelButton (int index, int lvlNo)
	{
		float posY = spacing + (spacing + height) * index;

		GameObject btn = (GameObject)Instantiate (levelButtonPrefab.gameObject);
		btn.SetActive(true);
		btn.name = (index+1).ToString ();
		btn.GetComponentInChildren<Text>().text = btn.name;
		RectTransform btnTransform = btn.GetComponent<RectTransform> ();
		btnTransform.SetParent (transform);
		btnTransform.localScale = Vector3.one;
		btnTransform.localRotation = Quaternion.identity;
		btnTransform.localPosition = Vector3.zero;
		//btnTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height);
		btnTransform.sizeDelta = new Vector2 (indent, height);
		btnTransform.anchoredPosition = new Vector2 (0, -posY);

		RectTransform currT = gameObject.GetComponent<RectTransform> ();
		Vector2 size = currT.sizeDelta;
		if (size.y <= posY) {
			size.y = posY + (height + spacing)*2;
			currT.sizeDelta = size;
		}

		btn.GetComponent<LevelSelectButton>().levelNo = lvlNo;

		return btn;
	}

	// Use this for initialization
	void Start () {
//		levelNames = new List<string>(SceneManager.sceneCountInBuildSettings);
//		getLevels();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (update) {
			update = false;
			levelNames = new List<string> (SceneManager.sceneCountInBuildSettings);
			getLevels ();
		}
		if (clear) {
			clear = false;
			removeButtons();
		}
	}
#endif
}
