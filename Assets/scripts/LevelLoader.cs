﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour {

	public bool autoIncrement = true;
	public string nextLevel;

	void Start ()
	{
		if (autoIncrement) {
			constructNextLevelString();
		}
	}

	private void constructNextLevelString ()
	{
		string levelName = SceneManager.GetActiveScene ().name;
		char[] characters = levelName.ToCharArray ();
		int ind = characters.Length - 1;

		while (ind > 0) {
//			print(characters[ind]);
			if (characters [ind] > '9') {
				ind++; //we passed all the numbers
				break;
			}
			ind--;
		}
		string ending = levelName.Substring(ind);
		string beginning = levelName.Substring(0, ind);
		int levelNo = System.Convert.ToInt32(ending);
		nextLevel = beginning + (levelNo+1);
//		print(nextLevel);
	}

	public void loadLevel ()
	{
		SceneManager.LoadScene(nextLevel);
	}

	public void loadLevel (string name)
	{
		SceneManager.LoadScene(name);
	}

	public void restartLevel ()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}
}
