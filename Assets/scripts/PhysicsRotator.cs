﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class PhysicsRotator : MonoBehaviour {

	public Vector3 angle;
	public float cooldown = 2, speed = 10, rewindDelay = 2, rewindSpeed = 2;
	public bool autoRewind = true;

	private Quaternion rotation, startRotation;
	private Rigidbody rb;

	void Awake ()
	{
		rb = GetComponent<Rigidbody>();
		rotation = Quaternion.Euler(angle);
		startRotation = transform.rotation;
	}

	// Use this for initialization
	void Start () {
		StartCoroutine(startCooldown(cooldown));
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	private IEnumerator startCooldown (float time)
	{
		while (time > 0) {
			time -= Time.deltaTime;
			yield return null;
		}
		StartCoroutine(play(speed));
	}

	private IEnumerator play (float speed)
	{
		float timer = 0;
		YieldInstruction inst = new WaitForFixedUpdate ();
		Quaternion curr = startRotation;
		while (timer < 1) {
			timer += Time.fixedDeltaTime * speed;
			curr = Quaternion.Lerp(startRotation, rotation, timer);
			rb.MoveRotation(curr);
			yield return inst;
		}

		if (autoRewind) {
			timer = 0;
			while (timer < rewindDelay) {
				timer += Time.deltaTime;
				yield return null;;
			}
			StartCoroutine(rewind(rewindSpeed));
		}
	}

	private IEnumerator rewind (float speed)
	{
		float timer = 0;
		YieldInstruction inst = new WaitForFixedUpdate ();
		Quaternion curr = rotation;
		while (timer < 1) {
			timer += Time.fixedDeltaTime * speed;
			curr = Quaternion.Lerp(rotation, startRotation, timer);
			rb.MoveRotation(curr);
			yield return inst;
		}

		StartCoroutine(startCooldown(cooldown));
	}
}
