﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.UI;

public class LevelTimer : MonoBehaviour {

	public float playTime = 60;
	public bool isTimed = false;
	public Text timerDisp;
	public Goal goal;
	public UnityEvent onComplete, onWin, onLose;

	// Use this for initialization
	void Start ()
	{
		if (!isTimed && timerDisp)
			timerDisp.gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (isTimed) {
			if (playTime > 0) {
				playTime -= Time.deltaTime;
				timerDisp.text = "" + (int)playTime;
			} else {
				endLevel ();
			}
		}
	}

	public void endLevel ()
	{
		onComplete.Invoke();
		if(goal.reachedGoal())
			onWin.Invoke();
		else
			onLose.Invoke();

		this.enabled = false;
	}
}
