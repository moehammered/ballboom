﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class PhysicsLerper : MonoBehaviour {

	public Transform pointA, pointB;
	public float cooldown = 2, speed = 10, rewindDelay = 2, rewindSpeed = 2;
	public bool autoRewind = true;

	private Vector3 startPoint, endPoint;
	private Rigidbody rb;

	void Awake ()
	{
		if(pointA == null)
			pointA = transform;
		if(pointB == null)
			Debug.LogError(this + ": Forgot the end point to lerp to!");
		rb = GetComponent<Rigidbody>();
		startPoint = pointA.position;
		endPoint = pointB.position;
	}

	// Use this for initialization
	void Start () {
		StartCoroutine(startCooldown(cooldown));
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	private IEnumerator startCooldown (float time)
	{
		while (time > 0) {
			time -= Time.deltaTime;
			yield return null;
		}
		StartCoroutine(play(speed));
	}

	private IEnumerator play (float speed)
	{
		float timer = 0;
		YieldInstruction inst = new WaitForFixedUpdate ();
		Vector3 currPos = startPoint;
		while (timer < 1) {
			timer += Time.fixedDeltaTime * speed;
			currPos = Vector3.Lerp (startPoint, endPoint, timer);
			rb.MovePosition (currPos);
			yield return inst;
		}

		if (autoRewind) {
			timer = 0;
			while (timer < rewindDelay) {
				timer += Time.deltaTime;
				yield return null;;
			}
			StartCoroutine(rewind(rewindSpeed));
		}
	}

	private IEnumerator rewind (float speed)
	{
		float timer = 0;
		YieldInstruction inst = new WaitForFixedUpdate ();
		Vector3 currPos = endPoint;
		while (timer < 1) {
			timer += Time.fixedDeltaTime * speed;
			currPos = Vector3.Lerp(endPoint, startPoint, timer);
			rb.MovePosition(currPos);
			yield return inst;
		}

		StartCoroutine(startCooldown(cooldown));
	}
}
