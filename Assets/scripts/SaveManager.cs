﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class SaveManager : MonoBehaviour {

	private const string completedLevelKey = "levels.completed.key";
	private const string highscoresKey = "levels.highscores.key";

	public int[] getHighScores ()
	{
		return SaveSerialiser.getInstance().loadData<int[]>(highscoresKey);
	}

	public int[] getCompletedLevels ()
	{
		return SaveSerialiser.getInstance().loadData<int[]>(completedLevelKey);
	}

	public void saveCompletedCurrentLevel ()
	{
		int no = extractLevelNumber ();
		if (no == -1)
			return;

		int[] completed = getCompletedLevels ();
		if (completed != null) {
			foreach (int i in completed) {
				if (i == no) {
					print ("Already beat this level");
					return;
				}
			}
			List<int> temp = new List<int> (completed);
			//we've reached the end, so it's not part of our already completed list
			temp.Add (no);
			SaveSerialiser.getInstance ().saveData (completedLevelKey, temp.ToArray ());
			print ("current level saved!");
		} else {
			print("First time saving!");
			completed = new int[1];
			completed[0] = no;
			SaveSerialiser.getInstance ().saveData (completedLevelKey, completed);
		}

	}

	public void saveCompletedLevel (int no)
	{
		List<int> completed = new List<int>(getCompletedLevels ());
		foreach (int i in completed) {
			if(i == no)
				return;
		}
		//we've reached the end, so it's not part of our already completed list
		completed.Add(no);
		SaveSerialiser.getInstance().saveData(completedLevelKey, completed.ToArray());
	}

	public bool saveHighScore (int score)
	{
		int lvlno = extractLevelNumber ();
		if (lvlno == -1)
			return false; //not in a valid lvl

		int[] scores = getHighScores ();
		//validate the score table exists
		if (scores != null) {
			//scores table exists, now check we've reached a level higher than this one
			if (scores.Length < lvlno) {
				//need to initialise the slots to fit
				print("Need to resize the score table from: " + scores.Length + " to " + lvlno);
				scores = initialiseScoreTable (scores, lvlno);
			}
		} else {
			//scores don't exist yet, time to make them up to this point.
			scores = initialiseScoreTable (null, lvlno);
			print("No score table found, made a new one with size: " + scores.Length);
		}
		//Score table is ready, now to check for a new high score
		if (scores [lvlno - 1] < score) {
			scores[lvlno-1] = score; //new highscore
			SaveSerialiser.getInstance().saveData(highscoresKey, scores);
			print("New high score: " + score);
			return true; //high score was written
		}
		
		return false; //high score was not written
	}

	private int[] initialiseScoreTable (int[] existing, int newRange)
	{
		List<int> newTable = new List<int> (newRange);
		if (existing != null) {
			newTable.InsertRange (0, existing);
		}

		for (int i = newTable.Count; i < newRange; i++) {
			newTable.Insert(i, 0);
		}

		return newTable.ToArray();
	}

	private int extractLevelNumber ()
	{
		string levelName = SceneManager.GetActiveScene ().name;
		char[] characters = levelName.ToCharArray ();
		int ind = characters.Length - 1;
		char currChar = ' ';
		while (ind > 0) {
			//print (characters [ind]);
			currChar = characters [ind];
			if (currChar >= 'A' && currChar <= 'z') {
				ind++;
				break;
			}
			ind--;
		}
		if(ind > characters.Length-1)
			return -1; //we didn't find the level number

		string ending = levelName.Substring(ind);
		print("Ending of level: " + ending);
		int levelNo = System.Convert.ToInt32(ending);

		return levelNo;
	}

	public void resetScores ()
	{
		PlayerPrefs.DeleteKey(highscoresKey);
		PlayerPrefs.Save();
	}

	public void resetCompletedLevels ()
	{
		PlayerPrefs.DeleteKey(completedLevelKey);
		PlayerPrefs.Save();
	}

	public void resetSave()
	{
		PlayerPrefs.DeleteAll();
		PlayerPrefs.Save();
		print("Save file reset.");
	}

	// Use this for initialization
	void Start ()
	{
		//saveCompletedCurrentLevel();
		//resetSave();
		int[] comp = getCompletedLevels ();
		if (comp == null)
			print ("No levels completed");
		else {
			print ("Levels have been completed");
			print(sizeof(int) * comp.Length + " Bytes used for level completion storage.");
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
