﻿using UnityEngine;
using System.Collections;

public class BoostPad : MonoBehaviour {

	public float force = 10;

	void OnTriggerEnter (Collider col)
	{
		Rigidbody rb = col.GetComponent<Rigidbody> ();
		if (rb) {
			rb.AddForce(transform.forward * force, ForceMode.VelocityChange);
		}
	}
}
