﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class PhysicsProp : MonoBehaviour {

	public bool wakeOnCollision = true;
	private Rigidbody rb;

	void Awake ()
	{
		rb = GetComponent<Rigidbody> ();
		if (wakeOnCollision) {
			rb.isKinematic = true;
		}
	}

	void OnCollisionEnter ()
	{
		if (wakeOnCollision) {
			wakeUp();
		}
	}

	public void wakeUp ()
	{
		rb.isKinematic = false;
		wakeOnCollision = false;
	}
}
