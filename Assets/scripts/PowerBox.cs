﻿using UnityEngine;
using System.Collections;

public class PowerBox : MonoBehaviour {

	public float highVal = 0.25f, lowVal = 0.1f, flashSpeed = 1;
	private Material[] materials;

	void Awake ()
	{
		Renderer[] renderers = GetComponentsInChildren<Renderer> ();
		materials = new Material[renderers.Length];
		for (int i = 0; i < renderers.Length; i++) {
			materials[i] = renderers[i].material;
		}
	}

	// Use this for initialization
	void Start () {
		StartCoroutine(flashPower(flashSpeed));
	}

	public void shutdown ()
	{
		StopAllCoroutines();
		print("shutting down");
		StartCoroutine(killPower(2));
	}

	private IEnumerator killPower (float duration)
	{
		float timer = 0;
		while (timer < 1) {
			timer += Time.deltaTime/duration;
			foreach (Material mat in materials) {
				lerpEmission(mat, highVal, lowVal, Color.black, timer);
				mat.color = Color.Lerp(Color.white, Color.black, Mathf.Clamp(timer, 0, 0.75f));
			}
			yield return null;
		} 
	}

	private IEnumerator flashPower (float flashSpeed)
	{
		float timer = 0;
		while (true) {
			timer += Time.deltaTime * flashSpeed;
			foreach (Material mat in materials) {
				lerpEmission(mat, lowVal, highVal, Color.white, Mathf.PingPong(timer, 1));
			}
			yield return null;
		}
	}

	private void lerpEmission (Material mat, float low, float high, Color targetCol, float time)
	{
		float intensity = Mathf.Lerp(low, high, time);
		mat.SetColor("_EmissionColor", targetCol * intensity);
	}
}
