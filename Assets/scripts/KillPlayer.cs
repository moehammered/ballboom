﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class KillPlayer : MonoBehaviour {

	public GameObject killEffectPrefab;
	public UnityEvent onKill;

	void OnTriggerEnter (Collider col)
	{
		checkPlayer(col.transform);
	}

	void OnCollisionEnter (Collision col)
	{
		checkPlayer(col.transform);
	}

	private void checkPlayer(Transform player)
	{
		if (player.tag == "Player") {
			GameObject obj = (GameObject)Instantiate(killEffectPrefab, player.position, Quaternion.identity);
			Destroy(obj, 1);
			Destroy(player.gameObject);
			onKill.Invoke();
		}
	}
}
