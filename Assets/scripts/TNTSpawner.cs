﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public class TNTSpawner : MonoBehaviour {

	public GameObject TNTPrefab;
	public Vector3 spawnOffset = Vector3.up * 10;
	public Text counterDisp;
	public int stock = 10;
	public ObjectSpawner playerSpawner;
	public UnityEvent onLastStock;

	private int startingStock = 0;

	// Use this for initialization
	void Start () {
		startingStock = stock;
		updateCounter();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public bool hasStock ()
	{
		return stock > 0;
	}

	public float percentageUsed ()
	{
		return (float)stock/(float)startingStock;
	}

	private void updateCounter ()
	{
		if (counterDisp) {
			counterDisp.text = "" + stock;
		}
	}

	private void checkForPlayerStop ()
	{
		print("I went boom");
		StartCoroutine(monitorPlayerSpeed(1));
	}

	public void spawnTNT (Vector3 point, float force, float radius)
	{
		if (stock > 0) {
			Vector3 spawnPoint = point + spawnOffset;
			GameObject obj = (GameObject)Instantiate (TNTPrefab, spawnPoint, Quaternion.identity);
			TNTExplosion tnt = obj.GetComponent<TNTExplosion> ();
			tnt.setupTNT (force, radius);
			stock--;
			updateCounter();
			if(stock == 0)
				tnt.onExplode = checkForPlayerStop;
		}
	}

	private IEnumerator monitorPlayerSpeed (float refreshRate)
	{
		yield return new WaitForFixedUpdate();
		if (playerSpawner.currentPlayer) {
			Rigidbody rb = playerSpawner.currentPlayer.GetComponent<Rigidbody> ();
			if (rb) {
				float stopSpeed = 2;
				while (rb && rb.velocity.sqrMagnitude > stopSpeed) {
					yield return new WaitForSeconds(refreshRate);
				}
				print("Player has stopped");
				onLastStock.Invoke();
			}
		}
	}
}
