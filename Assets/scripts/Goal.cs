﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.UI;

public class Goal : MonoBehaviour {

	public GameObject effectPrefab;
	public int goalCount = 0;
	public int targetGoals = 3;
	public Text goalDisp;
	public UnityEvent onGoal, onTarget;

	// Use this for initialization
	void Start () {
		goalDisp.text = goalCount + "/" + targetGoals;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public bool reachedGoal()
	{
		return goalCount >= targetGoals;
	}

	public void checkEndOfLevel ()
	{
		if(reachedGoal())
			onTarget.Invoke();
		else
			onGoal.Invoke ();
	}

	void OnCollisionEnter (Collision col)
	{
		if (col.transform.tag == "Player") {
			Destroy (col.gameObject);
			GameObject obj = (GameObject)Instantiate (effectPrefab, transform.position, effectPrefab.transform.rotation);
			Destroy (obj, 7);
			goalCount++;
			goalDisp.text = goalCount + "/" + targetGoals;
			checkEndOfLevel();
		}
	}
}
