﻿using UnityEngine;
using System.Collections;

public class MapSelect : MonoBehaviour {

	public float offset = 1065, animTime = 1;
	public int mapCount = 1, currMap = 0;
	private RectTransform rect;

	void Awake()
	{
		rect = GetComponent<RectTransform>();
	}

	public void swipeLeft ()
	{
		if (currMap < mapCount) {
			currMap++;
			shiftMaps();
		}
	}

	public void swipeRight ()
	{
		if (currMap > 0) {
			currMap--;
			shiftMaps();
		}
	}

	private void shiftMaps ()
	{
		StopAllCoroutines();
		StartCoroutine(tween(animTime, getTargetPosition()));
	}

	private Vector2 getTargetPosition ()
	{
		Vector2 pos = rect.anchoredPosition;
		pos.x = -offset * currMap;
		return pos;
	}

	private IEnumerator tween (float delay, Vector2 target)
	{
		float timer = 0;
		Vector2 startPos = rect.anchoredPosition;
		while (timer < 1) {
			timer += Time.deltaTime/delay;
			rect.anchoredPosition = Vector2.Lerp(startPos, target, timer);
			yield return null;
		}
	}
}
