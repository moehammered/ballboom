﻿using UnityEngine;
using System.Collections;

public class TNTExplosion : MonoBehaviour {

	public LayerMask targets;
	public GameObject explosionEffect;
	public float explosiveForce, explosiveRadius;
	public Renderer meshRenderer;
	public float explosionTimer = 1f;
	public bool useDirectionalForce = false;

	public delegate void ExplodeCallback();
	public ExplodeCallback onExplode;

	private bool hasLanded = false, isPrimed = false, rushDetonation = false;
	private new Rigidbody rigidbody;
	private Collider[] colliders;
	private Material meshMaterial;

	void Awake ()
	{
		rigidbody = GetComponent<Rigidbody> ();
		colliders = GetComponentsInChildren<Collider> ();
		foreach (Collider col in colliders) {
			col.enabled = false;
		}
		meshMaterial = meshRenderer.material;
	}

	// Use this for initialization
	void Start () {
		adjustSize();
		StartCoroutine(enableCollisions(1));
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		if (hasLanded && !isPrimed) {
			//disablePhysics();
			if(shouldArm())
				startExplosion();
		}
	}

	void OnCollisionEnter ()
	{
		hasLanded = true;
	}

	public void forceDetonate ()
	{
		rushDetonation = true;
	}

	public void setupTNT (float force, float radius)
	{
		explosiveForce = force;
		explosiveRadius = radius;
		adjustSize();
	}

	private void flashTNT ()
	{
		StartCoroutine(glowColor());
	}

	private bool shouldArm ()
	{
		if (rigidbody.velocity.sqrMagnitude < 2) {
			//rigidbody.isKinematic = true;
			return true;
			/*foreach (Collider col in colliders) {
				col.enabled = false;
			}*/
		}
		return false;
	}

	private void adjustSize ()
	{
		transform.localScale = Vector3.one * (explosiveRadius/3);
	}

	private void startExplosion ()
	{
		//print("Time for boom");
		isPrimed = true;
		StartCoroutine(countdown(explosionTimer));
		flashTNT();
	}

	private void explode ()
	{
		GameObject effect = (GameObject)Instantiate(explosionEffect, transform.position, Quaternion.identity);
		Destroy(effect, 2f);
		sweepForBodies(explosiveRadius);
		if(onExplode != null)
			onExplode();
		Destroy(gameObject, 0.1f);
	}

	private IEnumerator countdown (float timer)
	{
		Vector3 initialScale = transform.localScale;
		Vector3 smallSize = initialScale * 0.85f;
		while (timer > 0 && !rushDetonation) {
			timer -= Time.deltaTime;
			transform.localScale = Vector3.Lerp(initialScale, smallSize, Mathf.PingPong(timer, 1.25f-timer/explosionTimer));
			yield return null;
		}
		explode();
	}

	private IEnumerator enableCollisions (float delay)
	{
		while (delay > 0) {
			delay -= Time.deltaTime;
			yield return null;
		}
		foreach (Collider col in colliders) {
			col.enabled = true;
		}
	}

	private IEnumerator glowColor ()
	{
		float timer = 0, rate = 2, elapsedTime = 0, lerpLength = 0.5f;
		Color startColor = meshMaterial.color;
		Color endColor = Color.black;
		while (elapsedTime < explosionTimer) {
			timer += Time.deltaTime * rate;
			elapsedTime += Time.deltaTime;
			lerpLength = 1.25f - elapsedTime/explosionTimer;
			meshMaterial.color = Color.Lerp(startColor, endColor, Mathf.PingPong(timer, lerpLength));
			yield return null;
		}
	}

	private void sweepForBodies (float radius)
	{
		Collider[] objs = Physics.OverlapSphere (transform.position, radius, targets);
		//debugRadius(radius);
		foreach (Collider col in objs) {
			Rigidbody rb = col.GetComponent<Rigidbody> ();
			TNTExplosion tnt = col.GetComponentInParent<TNTExplosion> ();
			PhysicsProp prop = col.GetComponent<PhysicsProp> ();
			if (tnt) {
				tnt.forceDetonate ();
			} 
			else if (prop) {
				prop.wakeUp();
			}
			else if (rb) {
				if (useDirectionalForce) {
					Vector3 dir = rb.position - transform.position;
					//print(dir.normalized);
					rb.AddForce(dir.normalized * explosiveForce, ForceMode.Impulse);
				}
				else
					rb.AddExplosionForce(explosiveForce, transform.position, radius, 1, ForceMode.Impulse);
			}
		}
	}
}
